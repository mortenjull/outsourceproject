﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit.Search;
using MimeKit;
using RestSharp;


namespace OutSourceMailProject
{
    class Program
    {
        static string[] CommonSentFolderNames = {"Spam", /* maybe add some translated names */ };

        static void Main(string[] args)
        {
            string url;
            string userName;
            string password;
            string domain;

            Console.WriteLine("Please enter Url");
            url = Console.ReadLine();
            Console.WriteLine("Please enter username");
            userName = Console.ReadLine();
            Console.WriteLine("Please enter password");
            password = Console.ReadLine();
            Console.WriteLine("Please enter Domain");
            domain = Console.ReadLine();

            var task = Task.Factory.StartNew(() => MoniterMail(url, userName, password, domain));
            
            Console.ReadLine();
        }

        public static void MoniterMail(string url, string userName, string password, string monitorDomain)
        {
            while (true)
            {
                using (var client = new ImapClient())
                {
                    // For demo-purposes, accept all SSL certificates
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(url, 993, true);

                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    try
                    {
                        client.Authenticate(userName, password);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Login failed");
                        Console.ReadLine();
                        return;
                    }
                    

                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadOnly);
                    var inboxMessages = GetUnseenMessagesFromFolder(inbox, 1);

                    var spamfolder = GetFolder(client, new string[] { "Unwanted", "Spam" });
                    spamfolder.Open(FolderAccess.ReadOnly);                            
                    var spamMessages = GetUnseenMessagesFromFolder(spamfolder, 1);

                    var allmessages = inboxMessages.Concat(spamMessages).ToList();
                    client.Disconnect(true);

                    var restclient = new RestClient(monitorDomain);                   
                    
                    if(allmessages.Any())
                    {
                        var message = allmessages[0];
                        Console.WriteLine("Subject: {0}, Sender: {1}", message.Body, message.From);

                        var request = new RestRequest(@"notification/notify?message=”{message}”&sender=”{sender}”/", Method.POST);
                        request.AddUrlSegment("message", message.Subject + " Body " + message.Body + " attatchemnts " + message.Attachments.Any()); // replaces matching token in request.Resource
                        request.AddUrlSegment("sender", message.From); // replaces matching token in request.Resource

                        restclient.Execute(request);
                    }
                    else
                    {
                        var request = new RestRequest(@"notification/notify?message=”{message}”&sender=”{sender}”/", Method.POST);
                        request.AddUrlSegment("message", "null"); // replaces matching token in request.Resource
                        request.AddUrlSegment("sender", "null"); // replaces matching token in request.Resource

                        restclient.Execute(request);
                    }
                }

                Thread.Sleep(120000);
            }           
        }

        static List<MimeMessage> GetUnseenMessagesFromFolder(IMailFolder folder, int amount)
        {
            var messages = new List<MimeMessage>();

            if (folder == null)
                return messages;

            int count = 0;

            foreach (var uid in folder.Search(SearchQuery.NotSeen))
            {
                var message = folder.GetMessage(uid);

                messages.Add(message);
                count++;

                if (count == amount)
                {
                    return messages;
                }
            }

            return messages;
        }


        static IMailFolder GetFolder(ImapClient client, string[] folderNames)
        {
            var personal = client.GetFolder(client.PersonalNamespaces[0]);

            foreach (var folder in personal.GetSubfolders(false))
            {
                foreach (var name in folderNames)
                {
                    if (folder.Name.ToLowerInvariant() == name.ToLowerInvariant())
                        return folder;
                }              
            }

            return null;
        }
    }
}
